import {
  DATA_USER,
  REGISTER,
  UPDATE_MACHINE,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  LOGOUT_USER,
  CHANGE_PANE,
} from "./type";
const InitialState = {
  user: null,
  register: {
    getOtp: false,
  },
  activePane: 1,
  machine: {
    device_token: null,
    device_type: null,
    platform: null,
    latlong: null,
  },
  notification: null,
};

const User = (state = InitialState, action) => {
  switch (action.type) {
    case CHANGE_PANE:
      return {
        ...state,
        activePane: action.payload,
      };
    case DATA_USER:
      return {
        ...state,
        user: action.payload,
      };
    case REGISTER:
      return {
        ...state,
        register: action.payload,
      };
    case UPDATE_MACHINE:
      return {
        ...state,
        machine: action.payload,
      };
    case ADD_NOTIFICATION:
      return {
        ...state,
        notification: action.payload,
      };
    case REMOVE_NOTIFICATION:
      return { ...state, notification: null };
    case LOGOUT_USER:
      return {
        user: null,
        register: {
          getOtp: false,
        },
        machine: {
          device_token: null,
          device_type: null,
          platform: null,
          latlong: null,
        },
        notification: null,
      };
    default:
      return state;
  }
};

export default User;
