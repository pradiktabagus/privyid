import {
  DATA_USER,
  REGISTER,
  UPDATE_MACHINE,
  ADD_NOTIFICATION,
  REMOVE_NOTIFICATION,
  LOGOUT_USER,
  CHANGE_PANE,
} from "./type";

export const InitUser = (data) => {
  return { type: DATA_USER, payload: data };
};

export const RegisterUser = (data) => {
  return { type: REGISTER, payload: data };
};

export const UpdateMachine = (data) => {
  return { type: UPDATE_MACHINE, payload: data };
};
export const AddNotification = (data) => {
  return { type: ADD_NOTIFICATION, payload: data };
};
export const RemoveNotification = () => {
  return { type: REMOVE_NOTIFICATION };
};
export const LogoutUser = () => {
  return { type: LOGOUT_USER };
};
export const ChangePane = (data) => {
  return { type: CHANGE_PANE, payload: data };
};
