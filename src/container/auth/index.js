import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Login from "../../component/auth/login";
import Register from "../../component/auth/register";
import OTP from "../../component/auth/otp";
import { connect } from "react-redux";
import "./index.css";
import { ChangePane } from "../../redux/action";

const Panes = [
  { label: "Login", Component: Login, id: 1 },
  { label: "Registration", Component: Register, id: 2 },
  { label: "OTP", Component: OTP, id: 3, inVisible: true },
];
function Index(props) {
  const { onChangePane, activePane } = props;
  const [Component, setComponent] = useState(null);

  const changePane = (e, pane) => {
    e.preventDefault();
    onChangePane(pane);
  };

  useEffect(() => {
    let mount = true;
    if (mount) {
      let component = Panes.find((comp) => comp.id === activePane);
      setComponent(component.Component);
    }
  }, [activePane]);

  return (
    <div className="auth-page">
      <div className="form-auth">
        <div className="tabs">
          {Panes.filter((fil) => fil.inVisible !== true).map((item) => (
            <TabsPane
              label={item.label}
              aktiv={item.id === activePane.id}
              item={item}
              onChange={(e) => changePane(e, item.id)}
            />
          ))}
        </div>
        <div>{Component && <Component />}</div>
      </div>
    </div>
  );
}

function TabsPane({ label, aktiv, onChange }) {
  return (
    <div className={`tabpanes ${aktiv ? "active" : ""}`} onClick={onChange}>
      <a href="">{label}</a>
    </div>
  );
}

Index.propTypes = {
  props: PropTypes.any,
};

const mapStateToProps = ({ user }) => ({
  isGetOtp: user.register.getOtp,
  activePane: user.activePane,
});
const mapDispatchToProps = (dispatch) => ({
  onChangePane: (data) => {
    dispatch(ChangePane(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
