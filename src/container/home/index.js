import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  withRouter,
  Redirect,
} from "react-router-dom";
import Banner from "../../assets/img/banner.png";
import { ProfileMeController } from "../../api/profileController";
import Information from "../../component/home/Information";
import Education from "../../component/home/education";
import Gallery from "../../component/home/gallery";
import Career from "../../component/home/career";
import { AddNotification, InitUser } from "../../redux/action";
import { connect } from "react-redux";
import Cover from "../../component/home/banner";
import "./index.css";
import { ACCESS_TOKEN } from "../../helper/constanta";
function Index(props) {
  const { DataUser, initUser, AddNotif, history } = props;
  const [banner, setBanner] = useState(Banner);
  const [logout, setLogout] = useState(false);
  let { path } = useRouteMatch();

  const TabPanes = [
    { to: path, label: "Information", exact: true },
    { to: `${path}/career`, label: "Career", exact: false },
    { to: `${path}/education`, label: "Education", exact: false },
    { to: `/gallery`, label: "Gallery", exact: false },
  ];

  const getCurrentUser = () => {
    ProfileMeController()
      .then((response) => {
        setLogout(false);
        const dataUser = response.data.user;
        initUser(dataUser);
      })
      .catch((error) => {
        setLogout(true);
        localStorage.removeItem(ACCESS_TOKEN);
        AddNotif({ type: "warning", message: "Session End" });
      });
  };

  useEffect(() => {
    getCurrentUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (DataUser) {
      setBanner(DataUser.cover_picture.url);
    }
  }, [DataUser]);

  const handleChangeTab = (e) => {
    const { value } = e.target;
    const dataPanes = TabPanes.find((x) => x.label === value);
    history.push(dataPanes.to);
  };
  return logout ? (
    <Redirect
      to={{
        pathname: "/",
        state: { from: props.location },
      }}
    />
  ) : (
    <Router>
      <div className="section-profile">
        <div className="banner">
          <img src={banner} alt="banner" />
        </div>
        <div className="profile-page">
          <Cover />
          <div className="profile-information">
            <div className="header-profile">
              {DataUser ? (
                <React.Fragment>
                  <h3 className="profile-name">{DataUser.name}</h3>
                  <h5 className="more-info">{`Level ${DataUser.level} - #${DataUser.id}`}</h5>
                </React.Fragment>
              ) : null}
            </div>
            <div className="tabs-profile">
              {TabPanes.map((tab) => (
                <TabPane
                  activeOnlyWhenExact={tab.exact}
                  to={tab.to}
                  label={tab.label}
                />
              ))}
            </div>
            <div className="select-profile">
              <select defaultValue="Information" onChange={handleChangeTab}>
                {TabPanes.map((tab) => (
                  <option value={tab.label} key={tab.label}>
                    {tab.label}
                  </option>
                ))}
              </select>
            </div>
            <div className="content-profile">
              <Switch>
                <Route exact path={path}>
                  <Information />
                </Route>
                <Route path={`${path}/career`}>
                  <Career />
                </Route>
                <Route path={`${path}/education`}>
                  <Education />
                </Route>
                <Route path={`/gallery`}>
                  <Gallery />
                </Route>
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </Router>
  );
}

function TabPane({ label, to, activeOnlyWhenExact }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact,
  });
  return (
    <div className={`tabpanes ${match ? "active" : ""}`}>
      <Link to={to}>{label}</Link>
    </div>
  );
}

Index.propTypes = {
  props: PropTypes.any,
};

const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Index));
