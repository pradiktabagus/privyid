import { request } from "./request";

export function UploadProfileController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `uploads/profile`,
    method: "POST",
    body: datas,
  });
}

export function DeleteProfileController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `uploads/profile`,
    method: "DELETE",
    body: datas,
  });
}

export function ProfileDefaultController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `uploads/profile/default`,
    method: "POST",
    body: datas,
  });
}
export function CoverController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `uploads/cover`,
    method: "POST",
    body: datas,
  });
}
