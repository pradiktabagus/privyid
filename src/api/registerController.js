import { request } from "./request";

export function RegisterController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `register`,
    method: "POST",
    body: datas,
  });
}

export function RegisterOTPController(phone) {
  let datas = new FormData();
  for (const name in phone) {
    datas.append(name, phone[name]);
  }
  return request({
    url: `register/otp/request`,
    method: "POST",
    body: datas,
  });
}

export function RegisterMatchOTPController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `register/otp/match`,
    method: "POST",
    body: datas,
  });
}

export function RegisterRemoveController(phone) {
  let datas = new FormData();
  for (const name in phone) {
    datas.append(name, phone[name]);
  }
  return request({
    url: `register/remove`,
    method: "POST",
    body: datas,
  });
}
