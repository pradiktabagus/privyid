import { ACCESS_TOKEN } from "../helper/constanta";

export const request = (options) => {
  const headers = new Headers();

  if (localStorage.getItem(ACCESS_TOKEN)) {
    headers.append(
      "Authorization",
      "bearer " + localStorage.getItem(ACCESS_TOKEN)
    );
  }

  const defaults = {
    headers: headers,
  };
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options).then((response) =>
    response.json().then((json) => {
      if (!response.ok) {
        return Promise.reject(json);
      }
      return json;
    })
  );
};
