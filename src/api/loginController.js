import { request } from "./request";

export function LoginController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `oauth/sign_in`,
    method: "POST",
    body: datas,
  });
}
export function RevokeController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `oauth/revoke`,
    method: "POST",
    body: datas,
  });
}

export function UserMeController(param) {
  return request({
    url: `oauth/credentials?access_token=${param}`,
    method: "GET",
  });
}
