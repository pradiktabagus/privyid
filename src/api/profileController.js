import { request } from "./request";

export function ProfileMeController() {
  return request({
    url: `profile/me`,
    method: "GET",
  });
}

export function ProfileController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `profile`,
    method: "POST",
    body: datas,
  });
}
export function ProfileEducationController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `education`,
    method: "POST",
    body: datas,
  });
}

export function ProfileCareerController(body) {
  let datas = new FormData();
  for (const name in body) {
    datas.append(name, body[name]);
  }
  return request({
    url: `career`,
    method: "POST",
    body: datas,
  });
}
