import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import { InitUser, UpdateMachine } from "./redux/action";
import { connect } from "react-redux";
import { ACCESS_TOKEN } from "./helper/constanta";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Notification from "./component/notification/index";
import Auth from "./container/auth/index";
import Home from "./container/home/index";
import { ProfileMeController } from "./api/profileController";

function App(props) {
  const { UpdateMachine, notification, initUser, DataUser } = props;

  function getCoordinates() {
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject);
    });
  }

  async function getAddress() {
    // wait for the resolved result
    const position = await getCoordinates();
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;
    let url = `${latitude},${longitude}`;

    // Actually return a latlong
    return url;
  }

  async function getMachineID() {
    let device_type = 2;
    let device_token = "token";
    let platform = navigator.platform.toLowerCase();
    let latlong = await getAddress();
    UpdateMachine({
      device_type,
      platform,
      latlong,
      device_token,
    });
  }

  useEffect(() => {
    let mount = true;
    if (mount) {
      getMachineID();
      const token = window.localStorage.getItem(ACCESS_TOKEN);
      if (token) {
        getCurrentUser();
      }
    }
    return () => {
      mount = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getCurrentUser = () => {
    ProfileMeController()
      .then((response) => {
        const dataUser = response.data.user;
        initUser(dataUser);
      })
      .catch((error) => {
        localStorage.removeItem(ACCESS_TOKEN);
      });
  };

  return (
    <Router>
      <div className="App">
        <div className="inner-page">
          <aside className="sider">
            <div className="welcome-page">
              <h3>Welcome</h3>
              {notification ? (
                <Notification
                  type={notification.type}
                  caption={notification.message}
                />
              ) : null}
            </div>
          </aside>
          <main className="main-app">
            <Switch>
              <Route exact path="/">
                <Auth />
              </Route>
              <Route path="/profile" User={DataUser} component={Home} />
            </Switch>
          </main>
        </div>
      </div>
    </Router>
  );
}

const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
  notification: user.notification,
});

const mapsDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  UpdateMachine: (data) => {
    dispatch(UpdateMachine(data));
  },
});

export default connect(mapStateToProps, mapsDispatchToProps)(App);
