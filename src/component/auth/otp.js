import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import OtpInput from "react-otp-input";
import { withRouter } from "react-router-dom";
import {
  AddNotification,
  RegisterUser,
  RemoveNotification,
} from "../../redux/action";
import { connect } from "react-redux";
import Ic_resend from "../../assets/img/ic_resend.svg";
import {
  RegisterMatchOTPController,
  RegisterOTPController,
} from "../../api/registerController";

function OTP(props) {
  const { User, RegisUser, AddNotif, removeNotif, history } = props;
  const [resentOTP, setResentOTP] = useState(false);
  const [loading, setLoading] = useState(false);
  const [body, setBody] = useState({
    user_id: null,
    otp_code: null,
  });

  const handleChange = (otp) => {
    setBody((prevState) => ({
      ...prevState,
      otp_code: otp,
    }));
  };

  useEffect(() => {
    setBody((prevState) => ({
      ...prevState,
      user_id: User.sugar_id,
    }));
  }, [User]);

  const sendOTP = async () => {
    setResentOTP(true);
    removeNotif();
    const reqBody = {
      phone: User.phone,
    };
    const fetch = RegisterOTPController(reqBody);
    fetch
      .then((response) => {
        const initData = response.data.user;
        RegisUser({ ...initData, getOtp: true });
        AddNotif({
          type: "warning",
          message: `We sent OTP to ${initData.phone}`,
        });
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setResentOTP(false);
      });
  };

  const verifyOTP = (e) => {
    e.preventDefault();
    removeNotif();
    setLoading(true);
    RegisterMatchOTPController(body)
      .then((response) => {
        AddNotif({
          type: "success",
          message: `Thanks for being our member, please login to continue`,
        });
      })
      .then(() => {
        history.push("/");
      })
      .catch((error) => {
        AddNotif({
          type: "warning",
          message: `Try again`,
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <div className="section-otp">
      <div className="form-otp">
        <div className="title-form">
          OTP Verification
          <p>Insert OTP code sent to your phone</p>
        </div>
        <div className="otp">
          <OtpInput
            onChange={handleChange}
            value={body.otp_code}
            numInput={4}
            containerStyle={{
              width: "100%",
              justifyContent: "space-between",
              marginRight: 24,
            }}
            inputStyle={{
              height: 40,
              flex: 1,
              width: "100%",
              marginRight: 24,
              backgroundColor: "transparent",
              color: "#ffffff",
              border: "1px solid #52526B",
              fontSize: "24px",
              lineHeight: "24px",
            }}
          />
          <button className="btn-verify-otp" onClick={verifyOTP}>
            {loading ? "Loading..." : "Verify"}
          </button>
        </div>
      </div>
      <div className="space-otp">
        <img src={Ic_resend} alt="resend otp" />
        <button className="btn-resent-otp" onClick={sendOTP}>
          Resend OTP Code
        </button>
      </div>
    </div>
  );
}

OTP.propTypes = {};

const mapStateToProps = ({ user }) => ({
  User: user.register,
});

const mapsDispatchToProps = (dispatch) => ({
  RegisUser: (data) => {
    dispatch(RegisterUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
  removeNotif: () => {
    dispatch(RemoveNotification());
  },
});

export default connect(mapStateToProps, mapsDispatchToProps)(withRouter(OTP));
