import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { AddNotification, ChangePane, RegisterUser } from "../../redux/action";
import {
  RegisterController,
  RegisterOTPController,
} from "../../api/registerController";
const country = [{ code: 62, country: "indonesia", label: "Indonesia (+62)" }];
function Register(props) {
  const { RegisUser, machine, AddNotif, onChangePane } = props;
  const [loading, setLoading] = useState(false);
  const [body, setBody] = useState({
    phone: null,
    password: null,
    country: "indonesia",
    latlong: null,
    device_token: null,
    device_type: null,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    const fetch = RegisterController(body);
    fetch
      .then((response) => {
        sendOTP(e);
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.error.errors[0] });
      })
      .finally(() => setLoading(false));
  };

  const sendOTP = async (e) => {
    const reqBody = {
      phone: body.phone,
    };
    const fetch = RegisterOTPController(reqBody);
    fetch
      .then((response) => {
        const initData = response.data.user;
        RegisUser({ ...initData, getOtp: true });
        AddNotif({
          type: "warning",
          message: `We sent OTP to ${initData.phone}`,
        });
      })
      .then(() => {
        onChangePane(3);
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onChange = (e) => {
    const { name, value } = e.target;
    setBody((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  useEffect(() => {
    setBody((prevState) => ({
      ...prevState,
      latlong: machine.latlong,
      device_token: machine.device_token,
      device_type: machine.device_type,
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [machine]);

  const handleBlurPhoneNumber = (e) => {
    const { value } = e.target;
    const prefix = body.country;
    const dataPrefix = country.find((x) => x.country === prefix);
    setBody((prevState) => ({
      ...prevState,
      phone: `${dataPrefix.code}${value}`,
    }));
  };
  return (
    <div className="section-register">
      <div className="form-register">
        <div className="title-form">
          Create New Account
          <p>Before you can invest here, please create new account</p>
        </div>
        <form>
          <div className="title-form">Account Detail</div>
          <div className="field">
            <label>Select Country</label>
            <select className="field-country" defaultValue={body.country}>
              {country.map((cnt) => (
                <option
                  value={cnt.country}
                  key={cnt.code}
                  onChange={onChange}
                  name="country"
                >
                  {cnt.label}
                </option>
              ))}
            </select>
          </div>
          <div className="field">
            <label>Phone Number</label>
            <input
              className="field-input"
              onChange={onChange}
              name="phone"
              type="number"
              onBlur={handleBlurPhoneNumber}
            />
          </div>
          <div className="field">
            <label>Password</label>
            <input
              type="password"
              className="field-input"
              name="password"
              onChange={onChange}
            />
          </div>
        </form>
      </div>
      <div className="space-button">
        <button type="button" className="btn btn-reset">
          Reset
        </button>
        <button
          disable={loading}
          type="button"
          className="btn btn-register"
          onClick={handleSubmit}
        >
          {loading ? "Loading..." : "Register"}
        </button>
      </div>
    </div>
  );
}

Register.propTypes = {};

const mapStateToProps = ({ user }) => ({
  user,
  machine: user.machine,
  notification: user.notification,
});

const mapsDispatchToProps = (dispatch) => ({
  RegisUser: (data) => {
    dispatch(RegisterUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
  onChangePane: (data) => {
    dispatch(ChangePane(data));
  },
});

export default connect(
  mapStateToProps,
  mapsDispatchToProps
)(withRouter(Register));
