import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { AddNotification, InitUser } from "../../redux/action";
import { withRouter } from "react-router-dom";
import { ACCESS_TOKEN } from "../../helper/constanta";
import { connect } from "react-redux";
import { LoginController, UserMeController } from "../../api/loginController";

function Login(props) {
  const { history, initUser, machine, AddNotif } = props;
  const [loading, setLoading] = useState(false);
  const [body, setBody] = useState({
    phone: null,
    password: null,
    latlong: null,
    device_token: null,
    device_type: null,
  });
  useEffect(() => {
    setBody((prevState) => ({
      ...prevState,
      latlong: machine.latlong,
      device_token: machine.device_token,
      device_type: machine.device_type,
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [machine]);

  const handleLogin = () => {
    setLoading(true);
    LoginController(body)
      .then((response) => {
        const access_token = response.data.user.access_token;
        localStorage.setItem(ACCESS_TOKEN, access_token);
      })
      .then(() => {
        UserMe();
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: "Try Again" });
        setLoading(false);
      });
  };

  const UserMe = (params) => {
    UserMeController(params)
      .then((response) => {
        initUser(response.data.user);
      })
      .then(() => {
        history.push("/profile");
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: "Try Again" });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setBody((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  return (
    <div className="section-login">
      <div className="form-login">
        <div className="title-form">Login Account</div>
        <form>
          <div className="field">
            <label>Phone Number</label>
            <input
              className="field-input"
              name="phone"
              type="tel"
              onChange={handleChange}
            />
          </div>
          <div className="field">
            <label>Password</label>
            <input
              className="field-input"
              type="password"
              name="password"
              onChange={handleChange}
            />
          </div>
        </form>
      </div>
      <div className="space-button">
        <button type="button" className="btn btn-reset">
          Reset
        </button>
        <button type="button" className="btn btn-login" onClick={handleLogin}>
          {loading ? "Loading..." : "Login"}
        </button>
      </div>
    </div>
  );
}

Login.propTypes = {};
const mapStateToProps = ({ user }) => ({
  machine: user.machine,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
