import React, { useEffect } from "react";
import PropTypes from "prop-types";
import "./index.css";
import Ic_Success from "../../assets/img/ic_success.svg";
import IC_Close from "../../assets/img/ic_close.svg";
import Ic_Warning from "../../assets/img/ic_warning.svg";
import { connect } from "react-redux";
import { RemoveNotification } from "../../redux/action";
function Notification(props) {
  const { type, caption, removeNotification } = props;
  const IconType = type === "success" ? Ic_Success : Ic_Warning;

  const handleClose = (e) => {
    removeNotification();
  };

  useEffect(() => {
    const interval = setTimeout(() => {
      handleClose();
    }, 5000);
    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="res_notification">
      <img src={IconType} alt={type} />
      <div className="caption-notif">{caption}</div>
      <button className="btn-close" onClick={handleClose}>
        <img src={IC_Close} alt="close" />
      </button>
    </div>
  );
}

Notification.defaultProps = {
  type: "success" | "warning",
};
Notification.propTypes = {
  props: PropTypes.any,
  type: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
};
const mapDispatchToProps = (dispatch) => ({
  removeNotification: () => {
    dispatch(RemoveNotification());
  },
});
export default connect(null, mapDispatchToProps)(Notification);
