import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Ic_plus from "../../assets/img/ic_plus.svg";
import { connect } from "react-redux";
import { UploadProfileController } from "../../api/uploadController";
import { AddNotification, InitUser } from "../../redux/action";
function Gallery(props) {
  const { Gallery, initUser, AddNotif } = props;
  const [listGallery, setListGallery] = useState([]);
  useEffect(() => {
    setListGallery(Gallery);
  }, [Gallery]);

  const handleUpload = (event) => {
    const file = event.target.files[0];
    const body = {
      image: file,
    };
    UploadProfileController(body)
      .then((response) => {
        initUser(response.data.user);
      })
      .then(() => {
        AddNotif({ type: "success", message: "Success" });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.message });
      });
  };
  return (
    <div className="gallery-page">
      <div className="header-information">
        <h3 className="name-tabs">Gallery</h3>
        <h5 className="desc-tabs">Upload your special moment</h5>
        <input
          id="gallery"
          type="file"
          name="image"
          onChange={handleUpload}
          style={{ display: "none" }}
        />
        <label for="gallery" className="btn-edit-profile">
          <img src={Ic_plus} alt="add" />
        </label>
      </div>
      <div className="list-gallery">
        {listGallery.map((gal) => (
          <div className="item-gallery">
            <img src={gal.picture.url} alt={gal.id} />
          </div>
        ))}
      </div>
    </div>
  );
}

Gallery.propTypes = {};
const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
  Gallery: user.user ? user.user.user_pictures : [],
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
