import React, { useState } from "react";
import PropTypes from "prop-types";
import { CoverController } from "../../api/uploadController";
import { AddNotification, InitUser } from "../../redux/action";
import { connect } from "react-redux";
import { ACCESS_TOKEN } from "../../helper/constanta";
import { RevokeController } from "../../api/loginController";
import { withRouter } from "react-router-dom";
import Ic_camera from "../../assets/img/ic_camera.svg";
function Banner(props) {
  const { DataUser, initUser, AddNotif, history } = props;
  const [loadingBanner, setLoadingBanner] = useState(false);
  const [loadingLogout, setLoadingLogout] = useState(false);

  const uploadBanner = (event) => {
    const file = event.target.files[0];
    setLoadingBanner(true);
    const bodyFile = {
      image: file,
    };
    CoverController(bodyFile)
      .then((response) => {
        console.log(response);
        initUser({
          ...DataUser,
          cover_picture: response.data.user_picture.cover_picture,
        });
      })
      .then(() => {
        AddNotif({ type: "success", message: "Success" });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.message });
      })
      .finally(() => {
        setLoadingBanner(false);
      });
  };

  const handleLogout = (e) => {
    e.preventDefault();
    setLoadingLogout(true);
    const body = {
      access_token: localStorage.getItem(ACCESS_TOKEN),
      confirm: 1,
    };
    RevokeController(body)
      .then((response) => {
        history.push("/");
        localStorage.removeItem(ACCESS_TOKEN);
      })
      .then(() => {
        AddNotif({ type: "success", message: "Success" });
      })
      .catch((err) => {
        const { errors } = err.error;
        AddNotif({ type: "warning", message: errors[0] });
      })
      .finally(() => {
        setLoadingLogout(false);
      });
  };
  return (
    <div className="space-btn-cover">
      <input
        id="files"
        accept="image/*"
        type="file"
        style={{ display: "none" }}
        onChange={uploadBanner}
      />
      <div className="btn-change-cover">
        {loadingBanner ? (
          "Loading..."
        ) : (
          <label for="files">
            <img src={Ic_camera} alt="camera" />
            Change Cover
          </label>
        )}
      </div>
      <button className="btn-logout" onClick={handleLogout}>
        {loadingLogout ? "Loading..." : "Logout"}
      </button>
    </div>
  );
}

Banner.propTypes = {
  props: PropTypes.any,
};
const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Banner));
