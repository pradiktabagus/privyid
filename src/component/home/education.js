import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Ic_plus from "../../assets/img/ic_plus.svg";
import { ProfileEducationController } from "../../api/profileController";
import { AddNotification, InitUser } from "../../redux/action";
function Education(props) {
  const { School, AddNotif, initUser } = props;
  const [isEdit, setEdit] = useState(false);
  const [loading, setLoading] = useState(false);
  const [body, setBody] = useState({
    school_name: null,
    graduation_time: null,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setBody((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    ProfileEducationController(body)
      .then((response) => {
        initUser(response.data.user);
      })
      .then(() => {
        AddNotif({ type: "success", message: "success" });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: "Try again" });
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const handleEdit = (e) => {
    e.preventDefault();
    setEdit(!isEdit);
  };
  useEffect(() => {
    if (School) {
      setBody((prevState) => ({
        ...prevState,
        school_name: School.school_name,
        graduation_time: School.graduation_time,
      }));
    }
  }, [School]);
  return (
    <div className="education-page">
      <div className="header-information">
        <h3 className="name-tabs">Education Information</h3>
        <h5 className="desc-tabs">Information about your education</h5>
        <button className="btn-edit-profile" onClick={handleEdit}>
          <img src={Ic_plus} alt="add" />
        </button>
      </div>
      <div className="form-education">
        <form>
          <div className="field">
            <label>School Name</label>
            <input
              disabled={!isEdit}
              type="text"
              className="field-input"
              name="school_name"
              value={body.school_name}
              onChange={handleChange}
            />
          </div>
          <div className="field">
            <label for="graduation_time">Graduation Time</label>
            <input
              disabled={!isEdit}
              id="graduation_time"
              type="date"
              name="graduation_time"
              className="field-input"
              value={body.graduation_time}
              onChange={handleChange}
            />
          </div>
          {isEdit ? (
            <div className="space-button">
              <button
                type="button"
                className="btn btn-reset"
                onClick={handleEdit}
              >
                Discard
              </button>
              <button
                type="button"
                className="btn btn-career"
                onClick={handleSubmit}
              >
                {loading ? "Loading..." : "Add Education"}
              </button>
            </div>
          ) : null}
        </form>
        {School ? (
          <div className="list-career">
            <div className="list-item">
              <h4>{School.school_name}</h4>
              <h5>{School.graduation_time}</h5>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}

Education.propTypes = {};
const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
  School: user.user ? user.user.education : null,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Education);
