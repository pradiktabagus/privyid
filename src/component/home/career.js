import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Ic_plus from "../../assets/img/ic_plus.svg";
import { connect } from "react-redux";
import { ProfileCareerController } from "../../api/profileController";
import { AddNotification, InitUser } from "../../redux/action";
function Career(props) {
  const { AddNotif, Career } = props;
  const [loading, setLoading] = useState(false);
  const [isEdit, setEdit] = useState(false);
  const [body, setBody] = useState({
    position: null,
    company_name: null,
    starting_from: null,
    ending_in: null,
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setBody((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleEdit = (e) => {
    e.preventDefault();
    setEdit(!isEdit);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    ProfileCareerController(body)
      .then((response) => {
        AddNotif({ type: "success", message: "Success" });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.message });
      })
      .finally(() => {
        setLoading(false);
      });
  };
  useEffect(() => {
    if (Career)
      setBody((prevState) => ({
        ...prevState,
        company_name: Career.company_name,
        starting_from: Career.starting_from,
        ending_in: Career.ending_in,
      }));
  }, [Career]);
  return (
    <div className="career-page">
      <div className="header-information">
        <h3 className="name-tabs">Career Information</h3>
        <h5 className="desc-tabs">Information about your career</h5>
        <button className="btn-edit-profile" onClick={handleEdit}>
          <img src={Ic_plus} alt="add" />
        </button>
      </div>
      <div className="form-career">
        <form>
          <div className="field">
            <label>Company Name</label>
            <input
              disabled={!isEdit}
              type="text"
              className="field-input"
              name="company_name"
              value={body.company_name}
              onChange={handleChange}
            />
          </div>
          <div className="field">
            <label>Position</label>
            <input
              disabled={!isEdit}
              type="text"
              className="field-input"
              name="position"
              value={body.position}
              onChange={handleChange}
            />
          </div>
          <div className="field">
            <label for="starting_from">Start From</label>
            <input
              disabled={!isEdit}
              id="starting_from"
              type="date"
              name="starting_from"
              className="field-input"
              onChange={handleChange}
              value={body.starting_from}
            />
          </div>
          <div className="field">
            <label for="ending_in">Ending in</label>
            <input
              disabled={!isEdit}
              id="ending_in"
              type="date"
              name="ending_in"
              className="field-input"
              onChange={handleChange}
              value={body.ending_in}
            />
          </div>
          {isEdit ? (
            <div className="space-button">
              <button
                type="button"
                className="btn btn-reset"
                onClick={handleEdit}
              >
                Discard
              </button>
              <button
                type="button"
                className="btn btn-career"
                onClick={handleSubmit}
              >
                {loading ? "Loading..." : "Add Career"}
              </button>
            </div>
          ) : null}
        </form>
        {Career ? (
          <div className="list-career">
            <div className="list-item">
              <h4>{Career.company_name}</h4>
              <h5>{`${Career.starting_from} - ${Career.ending_in}`}</h5>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}

Career.propTypes = {};
const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
  Career: user.user ? user.user.career : null,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Career);
