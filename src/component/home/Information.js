import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { ProfileMeController } from "../../api/profileController";
import { AddNotification, InitUser } from "../../redux/action";
import Ic_pencil from "../../assets/img/ic_pencil.svg";
import ProfilePicture from "./profile-picture";

function Information(props) {
  const { DataUser } = props;
  const [isEdit, setIsEdit] = useState(false);
  const [loading, setLoading] = useState(false);
  const { AddNotif, initUser } = props;
  const [body, setBody] = useState({
    name: null,
    gender: 0,
    birthday: null,
    hometown: null,
    bio: null,
  });

  useEffect(() => {
    if (DataUser) {
      setBody((prevState) => ({
        ...prevState,
        name: DataUser.name,
        gender: DataUser.gender,
        birthday: DataUser.birthday,
        hometown: DataUser.hometown,
        bio: DataUser.bio,
      }));
    }
  }, [DataUser]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setBody((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    ProfileMeController(body)
      .then((response) => {
        initUser(response.data.user);
        AddNotif({ type: "success", message: "Data has been update" });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: "Try Again" });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleEdit = (e) => {
    e.preventDefault();
    setIsEdit(!isEdit);
  };
  return (
    <div className="information-page">
      <div className="header-information">
        <h3 className="name-tabs">Profile Information</h3>
        <h5 className="desc-tabs">Your Persondal Data</h5>
        <button className="btn-edit-profile" onClick={handleEdit}>
          <img src={Ic_pencil} alt="edit" />
        </button>
      </div>
      <div className="form-information">
        <div className="information-user">
          <div className="title-information">Product Detail</div>
          <form>
            <div className="field">
              <label>Name</label>
              <input
                disabled={!isEdit}
                type="text"
                className="field-input"
                name="name"
                value={body.name}
                onChange={handleChange}
              />
            </div>
            <div className="field">
              <label>Gender</label>
              <select
                disabled={!isEdit}
                className="field-input"
                defaultValue={0}
                onChange={handleChange}
                name="gender"
                value={body.gender}
              >
                <option key={0} value={0} onChange={handleChange} name="gender">
                  Male
                </option>
                <option key={1} value={1} onChange={handleChange} name="gender">
                  Female
                </option>
              </select>
            </div>
            <div className="field">
              <label for="birthday">Date of Birth</label>
              <input
                disabled={!isEdit}
                id="birthday"
                type="date"
                name="birthday"
                className="field-input"
                onChange={handleChange}
                value={body.birthday}
              />
            </div>
            <div className="field">
              <label>Wethon</label>
              <select disabled={!isEdit} className="field-input">
                <option key={1}>Cancer</option>
                <option key={2}>Aries</option>
              </select>
            </div>
            <div className="field">
              <label>Bio</label>
              <textarea
                disabled={!isEdit}
                name="bio"
                value={body.bio}
                onChange={handleChange}
                className="field-bio"
                placeholder="Write your bio here"
              />
            </div>
          </form>
        </div>
        <div className="information-picture">
          <div className="title-information">Profile Picture</div>
          <form>
            <ProfilePicture isEdit={isEdit} />
            <div className="field">
              <label>Address</label>
              <textarea
                disabled={!isEdit}
                value={body.hometown}
                className="field-bio"
                placeholder="Address"
                name="hometown"
                onChange={handleChange}
              />
            </div>
          </form>
        </div>
      </div>
      {isEdit ? (
        <div className="space-button">
          <button type="button" className="btn btn-reset" onClick={handleEdit}>
            Discard
          </button>
          <button
            type="button"
            className="btn btn-update"
            onClick={handleSubmit}
          >
            {loading ? "Loading..." : "Update"}
          </button>
        </div>
      ) : null}
    </div>
  );
}

Information.propTypes = {
  props: PropTypes.any,
};
const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Information);
