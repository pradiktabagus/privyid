import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  ProfileDefaultController,
  UploadProfileController,
} from "../../api/uploadController";
import { AddNotification, InitUser } from "../../redux/action";
import { connect } from "react-redux";

function ProfilePicture(props) {
  const { DataUser, isEdit, AddNotif, initUser } = props;
  const [profile, setProfile] = useState(null);
  const [loadingUpload, setLoadingUpload] = useState(false);
  useEffect(() => {
    if (DataUser) {
      setProfile(DataUser.user_picture?.picture.url);
    }
  }, [DataUser]);

  const uploadProfile = (event) => {
    const file = event.target.files[0];
    const body = {
      image: file,
    };
    setLoadingUpload(true);
    UploadProfileController(body)
      .then((response) => {
        setDefaultPicture(response.data);
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.message });
      })
      .finally(() => setLoadingUpload(false));
  };

  const setDefaultPicture = (picture) => {
    const body = {
      id: picture.user_picture.id,
    };
    ProfileDefaultController(body)
      .then((response) => {
        AddNotif({ type: "success", message: response.data });
      })
      .then(() => {
        initUser({
          ...DataUser,
          user_picture: picture.user_picture,
        });
      })
      .catch((error) => {
        AddNotif({ type: "warning", message: error.error.errors[0] });
      })
      .finally(() => {
        setLoadingUpload(false);
      });
  };
  return (
    <div className="field-photo">
      <img src={profile} id="photo-profile" alt="profile" />
      <input
        disabled={!isEdit}
        id="profile"
        accept="image/*"
        type="file"
        className="select-file"
        onChange={uploadProfile}
      />
      <div className="btn-media">
        {loadingUpload ? (
          "Loading..."
        ) : (
          <label for="profile">Upload Media</label>
        )}
      </div>
      <div className="desc-file">
        <p>PNG, JPG or MP4 up to 50MB</p>
      </div>
    </div>
  );
}

ProfilePicture.propTypes = {
  props: PropTypes.any,
};

const mapStateToProps = ({ user }) => ({
  DataUser: user.user,
});
const mapDispatchToProps = (dispatch) => ({
  initUser: (data) => {
    dispatch(InitUser(data));
  },
  AddNotif: (data) => {
    dispatch(AddNotification(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePicture);
