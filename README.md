# Pretest PrivyID

Project ini di bangun menggunakan [Create React App](https://github.com/facebook/create-react-app).

## Deskripsi Pretest yang di kerjakan

Pada Technical test ini mengimplementasikan dengan Framework CRA React JS dengan menggunakan dukungan:
- Redux
- Fetch
- React Hook
- React Router

## Technical Test yang di kerjakan

Feature yang dikerjakan pada technical test ini meliputi:
- User dapat Register dan Login dengan menggunakan nomor telepon
- User dapat menerima OTP untuk mengkonfirmasi nomor telepon dan permintaan ulang kode OTP
- User dapat merubah profile informasi data diri, career, education
- User dapat mengupload cover, profile picture dan terdapat menu gallery

## Technical Test yang di kerjakan
Feature yang belum di kerjakan
- Mendapatkan Device Token untuk pesan.

## Available Scripts

In the project directory, you can run:

### `npm start`

Untuk menjalankan aplikasi dapat mengakses.\
[http://localhost:3000](http://localhost:3000) dalam browser.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
